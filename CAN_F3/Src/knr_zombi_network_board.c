#include "knr_zombi_network_board.h"

ADC_HandleTypeDef    AdcCurrentSenseHandle;

void ErrorHandler(void) {
    HAL_GPIO_WritePin(LED3_PORT, LED3_2_PIN,GPIO_PIN_SET);
    __asm__ __volatile__ ("bkpt #0");//break to debugger
    while (1) {
        HAL_GPIO_TogglePin(LED3_PORT, LED3_2_PIN);
        HAL_Delay(100);
    }
}
void WarningHandler(void) {
    HAL_GPIO_WritePin(LED3_PORT, LED3_2_PIN,GPIO_PIN_SET);
    HAL_Delay(100);
    HAL_GPIO_WritePin(LED3_PORT, LED3_2_PIN,GPIO_PIN_RESET);
    HAL_Delay(100);
    HAL_GPIO_WritePin(LED3_PORT, LED3_2_PIN,GPIO_PIN_SET);
    HAL_Delay(500);
    HAL_GPIO_WritePin(LED3_PORT, LED3_2_PIN,GPIO_PIN_RESET);
    HAL_Delay(100);
    HAL_GPIO_WritePin(LED3_PORT, LED3_2_PIN,GPIO_PIN_SET);
    HAL_Delay(100);
    HAL_GPIO_WritePin(LED3_PORT, LED3_2_PIN,GPIO_PIN_RESET);
    HAL_Delay(100);
}


void LedInitAll(void) {
    GPIO_InitTypeDef  GPIO_InitStruct;
    //Clocks
    __HAL_RCC_GPIOB_CLK_ENABLE();
    __HAL_RCC_GPIOC_CLK_ENABLE();
    __HAL_RCC_GPIOF_CLK_ENABLE();
    //Pull up all diode pins
    GPIO_InitStruct.Mode  = GPIO_MODE_OUTPUT_PP;
    GPIO_InitStruct.Pull  = GPIO_PULLUP;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;

    GPIO_InitStruct.Pin = LED1_1_PIN|LED1_2_PIN;
    HAL_GPIO_Init(LED1_PORT, &GPIO_InitStruct);

    GPIO_InitStruct.Pin = LED2_1_PIN|LED2_2_PIN;
    HAL_GPIO_Init(LED2_PORT, &GPIO_InitStruct);

    GPIO_InitStruct.Pin = LED3_1_PIN|LED3_2_PIN;
    HAL_GPIO_Init(LED3_PORT, &GPIO_InitStruct);


    //Set all to off
    HAL_GPIO_WritePin(GPIOF, LED1_1_PIN|LED1_2_PIN,GPIO_PIN_RESET);
    HAL_GPIO_WritePin(GPIOB, LED2_1_PIN|LED2_2_PIN,GPIO_PIN_RESET);
    HAL_GPIO_WritePin(GPIOC, LED3_1_PIN|LED3_2_PIN,GPIO_PIN_RESET);
}

void ButtonInit(void) {
    GPIO_InitTypeDef GPIO_InitStruct;
    __HAL_RCC_GPIOA_CLK_ENABLE();
    //Button pin as input
    GPIO_InitStruct.Pin = BUTTON_PIN;
    GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;

    HAL_GPIO_Init(BUTTON_PORT, &GPIO_InitStruct);
}
uint8_t ButtonDown(void) {
    return !HAL_GPIO_ReadPin(BUTTON_PORT, BUTTON_PIN);
}


void AdcCurrentSenseInit(void) {
    GPIO_InitTypeDef          GPIO_InitStruct;

    ADC_CURRENT_SENSE_CLK_ENABLE();
    ADC_CURRENT_SENSE_CHANNEL_GPIO_CLK_ENABLE();

    GPIO_InitStruct.Pin = ADC_CURRENT_SENSE_CHANNEL_PIN;
    GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    HAL_GPIO_Init(ADC_CURRENT_SENSE_CHANNEL_GPIO_PORT, &GPIO_InitStruct);


    __HAL_RCC_ADC1_CONFIG(RCC_ADC1PCLK2_DIV8);


    /*##-1- Configure the ADC peripheral #######################################*/
    AdcCurrentSenseHandle.Instance          = ADC_CURRENT_SENSE;

    AdcCurrentSenseHandle.Init.ScanConvMode          = DISABLE;
    AdcCurrentSenseHandle.Init.ContinuousConvMode    = DISABLE;
    AdcCurrentSenseHandle.Init.DiscontinuousConvMode = DISABLE;
    AdcCurrentSenseHandle.Init.NbrOfDiscConversion   = 0;
    AdcCurrentSenseHandle.Init.ExternalTrigConv      = ADC_SOFTWARE_START;
    AdcCurrentSenseHandle.Init.DataAlign             = ADC_DATAALIGN_RIGHT;
    AdcCurrentSenseHandle.Init.NbrOfConversion       = 1;

    if (HAL_ADC_Init(&AdcCurrentSenseHandle) != HAL_OK) {
        ErrorHandler();
    }

    /*##-2- Configure ADC regular channel ######################################*/
    ADC_ChannelConfTypeDef sConfig;
    sConfig.Channel      = ADC_CURRENT_SENSE_CHANNEL;
    sConfig.Rank         = 1;
    sConfig.SamplingTime = ADC_SAMPLETIME_7CYCLES_5;

    if (HAL_ADC_ConfigChannel(&AdcCurrentSenseHandle, &sConfig) != HAL_OK) {
        ErrorHandler();
    }

}
uint32_t AdcCurrentSenseValue(void) {
    if (HAL_ADC_Start(&AdcCurrentSenseHandle) != HAL_OK) {
        ErrorHandler();
    }
    if (HAL_ADC_PollForConversion(&AdcCurrentSenseHandle, 10) != HAL_OK) {
        ErrorHandler();
    } else {
        return HAL_ADC_GetValue(&AdcCurrentSenseHandle);
    }
    return -1;
}
void CanInit(void) {
    GPIO_InitTypeDef   GPIO_InitStruct;

    /*##-1- Enable peripherals and GPIO Clocks #################################*/
    /* CAN1 Periph clock enable */
    CANx_CLK_ENABLE();
    /* Enable GPIO clock ****************************************/
    CANx_GPIO_CLK_ENABLE();

    /*##-2- Configure peripheral GPIO ##########################################*/
    /* CAN1 TX GPIO pin configuration */
    GPIO_InitStruct.Pin = CANx_TX_PIN;
    GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
    GPIO_InitStruct.Pull = GPIO_PULLUP;
    GPIO_InitStruct.Alternate =  CANx_TX_AF;

    HAL_GPIO_Init(CANx_TX_GPIO_PORT, &GPIO_InitStruct);

    /* CAN1 RX GPIO pin configuration */
    GPIO_InitStruct.Pin = CANx_RX_PIN;
    GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
    GPIO_InitStruct.Pull = GPIO_PULLUP;
    GPIO_InitStruct.Alternate =  CANx_RX_AF;

    HAL_GPIO_Init(CANx_RX_GPIO_PORT, &GPIO_InitStruct);

    /*##-3- Configure the NVIC #################################################*/
    /* NVIC configuration for CAN1 Reception complete interrupt */
    HAL_NVIC_SetPriority(CANx_RX_IRQn, 1, 0);
    HAL_NVIC_EnableIRQ(CANx_RX_IRQn);
}
