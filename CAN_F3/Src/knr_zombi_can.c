#include "knr_zombi_can.h"

extern CAN_HandleTypeDef    CanHandle;

uint8_t CanConfig(CAN_TypeDef* can)
{
    CAN_FilterConfTypeDef  sFilterConfig;
    static CanTxMsgTypeDef        TxMessage;
    static CanRxMsgTypeDef        RxMessage;

    /*##-1- Configure the CAN peripheral #######################################*/
    CanHandle.Instance = can;
    CanHandle.pTxMsg = &TxMessage;
    CanHandle.pRxMsg = &RxMessage;

    CanHandle.Init.TTCM = DISABLE;
    CanHandle.Init.ABOM = DISABLE;
    CanHandle.Init.AWUM = DISABLE;
    CanHandle.Init.NART = DISABLE;
    CanHandle.Init.RFLM = DISABLE;
    CanHandle.Init.TXFP = DISABLE;
    CanHandle.Init.Mode = CAN_MODE_NORMAL;
    CanHandle.Init.SJW = CAN_SJW_1TQ;
    CanHandle.Init.BS1 = CAN_BS1_6TQ;
    CanHandle.Init.BS2 = CAN_BS2_8TQ;
    CanHandle.Init.Prescaler = 2;

    if (HAL_CAN_Init(&CanHandle) != HAL_OK)
    {
        /* Initiliazation Error */
        WarningHandler();
        return 0;
    }

    /*##-2- Configure the CAN Filter ###########################################*/
    sFilterConfig.FilterNumber = 0;
    sFilterConfig.FilterMode = CAN_FILTERMODE_IDMASK;
    sFilterConfig.FilterScale = CAN_FILTERSCALE_32BIT;
    sFilterConfig.FilterIdHigh = 0x0000;
    sFilterConfig.FilterIdLow = 0x0000;
    sFilterConfig.FilterMaskIdHigh = 0x0000;
    sFilterConfig.FilterMaskIdLow = 0x0000;
    sFilterConfig.FilterFIFOAssignment = 0;
    sFilterConfig.FilterActivation = ENABLE;
    sFilterConfig.BankNumber = 14;

    if (HAL_CAN_ConfigFilter(&CanHandle, &sFilterConfig) != HAL_OK)
    {
        /* Filter configuration Error */
        WarningHandler();
        return 0;
    }

    /*##-3- Configure Transmission process #####################################*/
    CanHandle.pTxMsg->StdId = 0x321;
    CanHandle.pTxMsg->ExtId = 0x01;
    CanHandle.pTxMsg->RTR = CAN_RTR_DATA;
    CanHandle.pTxMsg->IDE = CAN_ID_STD;
    return 1;
}


