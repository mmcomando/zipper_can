#include "main.h"
#include "knr_zombi_can_protocol.h"
#include <stdio.h>
#include <stdlib.h>

CAN_HandleTypeDef    CanHandle;

static void SystemClock_Config(void);
void LED_Display(uint8_t LedStatus);
volatile int error=0;
int fputc(int c, FILE *stream)
{
    return ITM_SendChar(c);
}
int CAN_TryReconnect()
{
    uint8_t ret;
    if(CanHandle.State==HAL_CAN_STATE_TIMEOUT || CanHandle.State==HAL_CAN_STATE_ERROR   || CanHandle.State==HAL_CAN_STATE_RESET   )
    {
        ret=CanConfig(CANx);
        if(ret!=1)return 0;;
    }

    ret=HAL_CAN_Receive_IT(&CanHandle, CAN_FIFO0);

    if (ret != HAL_OK && ret != HAL_BUSY)
    {
        WarningHandler();
        return 0;
    }

    return 1;

}
int main(void)
{
    HAL_Init();
    SystemClock_Config();

    LedInitAll();
    ButtonInit();
    AdcCurrentSenseInit();
    CanInit();
    //HAL_GPIO_WritePin(LED1_1_PORT, LED1_1_PIN,GPIO_PIN_SET);
    //HAL_GPIO_WritePin(LED1_2_PORT, LED1_2_PIN,GPIO_PIN_SET);
    //HAL_GPIO_WritePin(LED2_1_PORT, LED2_1_PIN,GPIO_PIN_SET);//Dont Work(manufacturing flaw?)
    //HAL_GPIO_WritePin(LED2_2_PORT, LED2_2_PIN,GPIO_PIN_SET);//Dont Work(manufacturing flaw?)
    //HAL_GPIO_WritePin(LED3_1_PORT, LED3_1_PIN,GPIO_PIN_SET);//Dont damage during soldering
    //HAL_GPIO_WritePin(LED3_PORT, LED3_2_PIN,GPIO_PIN_SET);
    volatile int zmiany=0;
    volatile uint8_t current=0;
    int last=0;
    /* while(1) {
         current=AdcCurrentSenseValue();
         uint8_t buttonStatus=ButtonDown();
         HAL_GPIO_WritePin(LED1_PORT,LED1_1_PIN,buttonStatus);
         if(last!=buttonStatus)zmiany++;
         last=buttonStatus;
     }
     ErrorHandler();*/

    /*##-2- Start the Reception process and enable reception interrupt #########*/


    /* if (HAL_CAN_Receive_IT(&CanHandle, CAN_FIFO0)!= HAL_OK )
     {
         WarningHandler();
     }*/
    /* Infinite loop */
    while (1)
    {
        //current=AdcCurrentSenseValue();
        uint8_t ret=CAN_TryReconnect();
        if(!ret)
        {
            continue;
        }

        if(error)
        {
            HAL_GPIO_WritePin(LED1_PORT, LED1_2_PIN,GPIO_PIN_SET);
            HAL_Delay(50);
            HAL_GPIO_WritePin(LED1_PORT, LED1_2_PIN,GPIO_PIN_RESET);
            HAL_Delay(50);
            HAL_GPIO_WritePin(LED1_PORT, LED1_2_PIN,GPIO_PIN_SET);
            HAL_Delay(50);
            HAL_GPIO_WritePin(LED1_PORT, LED1_2_PIN,GPIO_PIN_RESET);
            HAL_Delay(50);
            HAL_GPIO_WritePin(LED1_PORT, LED1_2_PIN,GPIO_PIN_SET);
            HAL_Delay(50);
            HAL_GPIO_WritePin(LED1_PORT, LED1_2_PIN,GPIO_PIN_RESET);
            HAL_Delay(50);
            error=0;
            /* while (HAL_CAN_Receive_IT(&CanHandle, CAN_FIFO0) != HAL_OK) {

                    while(!CanConfig(CANx)) {
                        //HAL_GPIO_WritePin(LED1_PORT, LED1_2_PIN,GPIO_PIN_RESET);
                        WarningHandler();
                    }
                }*/
        }

        HAL_GPIO_TogglePin(LED1_PORT, LED1_1_PIN);
        HAL_Delay(500);
    }

}


/**
  * @brief  System Clock Configuration
  *         The system Clock is configured as follow :
  *            System Clock source            = PLL (HSE)
  *            SYSCLK(Hz)                     = 72000000
  *            HCLK(Hz)                       = 72000000
  *            AHB Prescaler                  = 1
  *            APB1 Prescaler                 = 2
  *            APB2 Prescaler                 = 1
  *            HSE Frequency(Hz)              = 8000000
  *            HSE PREDIV                     = 1
  *            PLLMUL                         = RCC_PLL_MUL9 (9)
  *            Flash Latency(WS)              = 2
  * @param  None
  * @retval None
  */
static void SystemClock_Config(void)
{
    RCC_ClkInitTypeDef RCC_ClkInitStruct;
    RCC_OscInitTypeDef RCC_OscInitStruct;

    /* Enable HSE Oscillator and activate PLL with HSE as source */
    RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
    RCC_OscInitStruct.HSEState = RCC_HSE_ON;
    RCC_OscInitStruct.HSEPredivValue = RCC_HSE_PREDIV_DIV1;
    RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
    RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
    RCC_OscInitStruct.PLL.PLLMUL = RCC_PLL_MUL9;
    if (HAL_RCC_OscConfig(&RCC_OscInitStruct)!= HAL_OK)
    {
        ErrorHandler();
    }

    /* Select PLL as system clock source and configure the HCLK, PCLK1 and PCLK2
       clocks dividers */
    RCC_ClkInitStruct.ClockType = (RCC_CLOCKTYPE_SYSCLK | RCC_CLOCKTYPE_HCLK | RCC_CLOCKTYPE_PCLK1 | RCC_CLOCKTYPE_PCLK2);
    RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
    RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
    RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
    RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;
    if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_2)!= HAL_OK)
    {
        ErrorHandler();
    }
}



/**
  * @brief  Transmission  complete callback in non blocking mode
  * @param  CanHandle: pointer to a CAN_HandleTypeDef structure that contains
  *         the configuration information for the specified CAN.
  * @retval None
  */
void HAL_CAN_RxCpltCallback(CAN_HandleTypeDef *CanHandle)
{
    CanRxMsgTypeDef* msg=CanHandle->pRxMsg;
    msg=CanHandle->pRxMsg;



    if ((CanHandle->pRxMsg->StdId == 0x321) && (CanHandle->pRxMsg->IDE == CAN_ID_STD) && (CanHandle->pRxMsg->DLC == 2))
    {

        //LED_Display(CanHandle->pRxMsg->Data[0]);
//        ubKeyNumber = CanHandle->pRxMsg->Data[0];
    }
    uint16_t length;
    uint16_t* data=receiveData(CanHandle,&length);
    if(data!=NULL)
    {
        uint32_t data_watch[16];//used to debug reveived bytes
        memcpy(&data_watch[0],data,length);
        free(data);
    }

    HAL_GPIO_WritePin(LED1_PORT, LED1_2_PIN,GPIO_PIN_SET);
    if (HAL_CAN_Receive_IT(CanHandle, CAN_FIFO0) != HAL_OK)
    {
        ErrorHandler();
    }
}

void HAL_CAN_ErrorCallback(CAN_HandleTypeDef *CanHandle)
{
    error=1;
}

void LED_Display(uint8_t LedStatus)
{
    HAL_GPIO_WritePin(LED1_PORT, LED1_1_PIN,GPIO_PIN_RESET);
    HAL_GPIO_WritePin(LED1_PORT, LED1_2_PIN,GPIO_PIN_RESET);
    HAL_GPIO_WritePin(LED3_PORT, LED3_2_PIN,GPIO_PIN_RESET);

    switch(LedStatus)
    {
    case (1):
        HAL_GPIO_WritePin(LED1_PORT, LED1_1_PIN,GPIO_PIN_SET);
        break;

    case (2):
//s        HAL_GPIO_WritePin(LED1_PORT, LED1_2_PIN,GPIO_PIN_SET);
        break;

    case (3):
        HAL_GPIO_WritePin(LED3_PORT, LED3_2_PIN,GPIO_PIN_SET);
        break;
    default:
        break;
    }
}

#ifdef  USE_FULL_ASSERT

/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
    /* User can add his own implementation to report the file name and line number,
       ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */

    /* Infinite loop */
    while (1)
    {
    }
}

#endif

/**
  * @}
  */

/**
  * @}
  */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
