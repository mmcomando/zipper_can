#ifndef KNR_ZOMBI_NETWOR_BOARD_H_INCLUDED
#define KNR_ZOMBI_NETWOR_BOARD_H_INCLUDED

#include "stm32f3xx_hal.h"
//CAN
#define CANx                            CAN
#define CANx_CLK_ENABLE()               __HAL_RCC_CAN1_CLK_ENABLE()
#define CANx_GPIO_CLK_ENABLE()          __HAL_RCC_GPIOA_CLK_ENABLE()

#define CANx_FORCE_RESET()              __HAL_RCC_CAN1_FORCE_RESET()
#define CANx_RELEASE_RESET()            __HAL_RCC_CAN1_RELEASE_RESET()

#define CANx_TX_PIN                    GPIO_PIN_12
#define CANx_TX_GPIO_PORT              GPIOA
#define CANx_TX_AF                     GPIO_AF9_CAN
#define CANx_RX_PIN                    GPIO_PIN_11
#define CANx_RX_GPIO_PORT              GPIOA
#define CANx_RX_AF                     GPIO_AF9_CAN

#define CANx_RX_IRQn                   CAN_RX0_IRQn
#define CANx_RX_IRQHandler             CAN_RX0_IRQHandler


///ADC
#define ADC_CURRENT_SENSE                          ADC1
#define ADC_CURRENT_SENSE_CLK_ENABLE()               __HAL_RCC_ADC1_CLK_ENABLE()
#define ADC_CURRENT_SENSE_CHANNEL_GPIO_CLK_ENABLE()  __HAL_RCC_GPIOA_CLK_ENABLE()

#define ADC_CURRENT_SENSE_FORCE_RESET()              __HAL_RCC_ADC1_FORCE_RESET()
#define ADC_CURRENT_SENSE_RELEASE_RESET()            __HAL_RCC_ADC1_RELEASE_RESET()

#define ADC_CURRENT_SENSE_CHANNEL_PIN                GPIO_PIN_4
#define ADC_CURRENT_SENSE_CHANNEL_GPIO_PORT          GPIOA
#define ADC_CURRENT_SENSE_CHANNEL                    ADC_CHANNEL_4


#define     LED1_1_PIN  GPIO_PIN_6
#define     LED1_PORT GPIOF
#define     LED1_2_PIN  GPIO_PIN_7

#define     LED2_1_PIN  GPIO_PIN_4
#define     LED2_PORT GPIOB
#define     LED2_2_PIN  GPIO_PIN_5

#define     LED3_1_PIN  GPIO_PIN_13
#define     LED3_PORT GPIOC
#define     LED3_2_PIN  GPIO_PIN_14

#define     BUTTON_PIN  GPIO_PIN_2
#define     BUTTON_PORT GPIOA



extern ADC_HandleTypeDef    AdcCurrentSenseHandle;

void LedInitAll(void);
void ButtonInit(void);
void CanInit(void);
void AdcCurrentSenseInit(void);
uint32_t AdcCurrentSenseValue(void);
uint8_t ButtonDown(void);
void ErrorHandler(void);
void WarningHandler(void);

#endif // KNR_ZOMBI_NETWOR_BOARD_H_INCLUDED
