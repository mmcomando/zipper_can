#ifndef KNR_ZOMBI_CAN_H_INCLUDED
#define KNR_ZOMBI_CAN_H_INCLUDED

#include "stm32f3xx_hal.h"

extern CAN_HandleTypeDef    CanHandle;
uint8_t CanConfig(CAN_TypeDef* can);
#endif // KNR_ZOMBI_CAN_H_INCLUDED
