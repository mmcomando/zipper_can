#include "main.h"
#include "knr_zombi_can_protocol.h"

#define KEY_PRESSED     0x00
#define KEY_NOT_PRESSED 0x01

uint8_t ubKeyNumber = 0x0;
CAN_HandleTypeDef    CanHandle;

/* Private function prototypes -----------------------------------------------*/
static uint8_t CAN_Config(void);
static void SystemClock_Config(void);
static void Error_Handler(void);
static void LED_Display(uint8_t Ledstatus);



volatile int asd;
uint32_t getAPB1Clock()
{
    RCC_ClkInitTypeDef clkInit;
    uint32_t flashLatency;
    HAL_RCC_GetClockConfig(&clkInit, &flashLatency);

    uint32_t hclkClock = HAL_RCC_GetHCLKFreq();
    uint8_t clockDivider = 1;
    if (clkInit.APB1CLKDivider == RCC_HCLK_DIV1)
        clockDivider = 1;
    if (clkInit.APB1CLKDivider == RCC_HCLK_DIV2)
        clockDivider = 2;
    if (clkInit.APB1CLKDivider == RCC_HCLK_DIV4)
        clockDivider = 4;
    if (clkInit.APB1CLKDivider == RCC_HCLK_DIV8)
        clockDivider = 8;
    if (clkInit.APB1CLKDivider == RCC_HCLK_DIV16)
        clockDivider = 16;

    uint32_t apb1Clock = hclkClock / clockDivider;
    return apb1Clock;
}


int CAN_TryReconnect()
{
    uint8_t ret;
    if(CanHandle.State==HAL_CAN_STATE_TIMEOUT || CanHandle.State==HAL_CAN_STATE_ERROR   || CanHandle.State==HAL_CAN_STATE_RESET   )
    {
        ret=CAN_Config();
        if(ret!=1)return 0;;
    }

    ret=HAL_CAN_Receive_IT(&CanHandle, CAN_FIFO0);

    if (ret != HAL_OK && ret != HAL_BUSY)
    {
        return 0;
    }

    return 1;

}
int main(void)
{
    /* STM32F4xx HAL library initialization:
         - Configure the Flash prefetch, instruction and Data caches
         - Configure the Systick to generate an interrupt each 1 msec
         - Set NVIC Group Priority to 4
         - Global MSP (MCU Support Package) initialization
       */
    HAL_Init();

    /* Configure the system clock to 168 MHz */
    SystemClock_Config();

    /* Configure LED1, LED2, LED3 and LED4 */
    BSP_LED_Init(LED1);
    BSP_LED_Init(LED2);
    BSP_LED_Init(LED3);
    BSP_LED_Init(LED4);
    asd=getAPB1Clock();

    /* Configure Key Button */
    BSP_PB_Init(BUTTON_KEY, BUTTON_MODE_GPIO);


    BSP_LED_On(1);

    uint32_t tmp_data[7]= {1,2,3,4,5,6,7};

    while(1)
    {
        uint8_t ret;
        do
        {
            ret=CAN_TryReconnect();
        }
        while(ret==0);
        //current=AdcCurrentSenseValue();

        CAN_Message* msg=popTailMessage();
        if(msg!=NULL)
        {
            uint32_t data_watch[16];//used to debug reveived bytes
            memcpy(&data_watch[0],msg->data,msg->length);
            uint8_t ok;
            do
            {
                ok=sendData(msg->data,msg->length);
                if(!ok)
                {
                    //Error_Handler();
                    HAL_GPIO_TogglePin(GPIOC,GPIO_PIN_13);
                    HAL_Delay(100);
                    HAL_GPIO_TogglePin(GPIOC,GPIO_PIN_13);
                    HAL_Delay(100);
                    HAL_GPIO_TogglePin(GPIOC,GPIO_PIN_13);
                    HAL_Delay(100);
                    HAL_GPIO_TogglePin(GPIOC,GPIO_PIN_13);
                    HAL_Delay(100);
                    do
                    {
                        ret=CAN_TryReconnect();
                    }
                    while(ret==0);
                }
            }
            while(ok==0);
            removeMessage(msg);
            HAL_GPIO_TogglePin(GPIOC,GPIO_PIN_13);
            HAL_Delay(1000);
            HAL_GPIO_TogglePin(GPIOC,GPIO_PIN_13);
            HAL_Delay(1000);

        }
        else
        {
            addMessage(&tmp_data[0],4,NULL);
            addMessage(&tmp_data[1],4,NULL);
            addMessage(&tmp_data[2],4,NULL);
            addMessage(&tmp_data[3],4,NULL);
            addMessage(&tmp_data[4],4,NULL);
            addMessage(&tmp_data[5],4,NULL);
            addMessage(&tmp_data[6],4,NULL);
        }


        HAL_GPIO_TogglePin(GPIOC,GPIO_PIN_13);
        HAL_Delay(500);

    }
}
void BSP_LED_Init(uint32_t a )
{

    GPIO_InitTypeDef  gpio_init_structure;

    __GPIOC_CLK_ENABLE();

    gpio_init_structure.Pin = GPIO_PIN_13;
    gpio_init_structure.Mode = GPIO_MODE_OUTPUT_PP;
    gpio_init_structure.Pull = GPIO_PULLUP;
    gpio_init_structure.Speed = GPIO_SPEED_HIGH;

    HAL_GPIO_Init(GPIOC, &gpio_init_structure);
}
void BSP_PB_Init(uint32_t a,uint32_t b) {}
void BSP_LED_Off(uint32_t a)
{
    HAL_GPIO_WritePin(GPIOC, GPIO_PIN_13,GPIO_PIN_RESET);
}
void BSP_LED_On(uint32_t a)
{
    HAL_GPIO_WritePin(GPIOC, GPIO_PIN_13,GPIO_PIN_SET);
}
uint32_t BSP_PB_GetState(uint32_t a)
{
    return 0;
}
/**
  * @brief  System Clock Configuration
  *         The system Clock is configured as follow :
  *            System Clock source            = PLL (HSE)
  *            SYSCLK(Hz)                     = 168000000
  *            HCLK(Hz)                       = 168000000
  *            AHB Prescaler                  = 1
  *            APB1 Prescaler                 = 4
  *            APB2 Prescaler                 = 2
  *            HSE Frequency(Hz)              = 25000000
  *            PLL_M                          = 25
  *            PLL_N                          = 336
  *            PLL_P                          = 2
  *            PLL_Q                          = 7
  *            VDD(V)                         = 3.3
  *            Main regulator output voltage  = Scale1 mode
  *            Flash Latency(WS)              = 5
  * @param  None
  * @retval None
  */
static void SystemClock_Config(void)
{
    RCC_ClkInitTypeDef RCC_ClkInitStruct;
    RCC_OscInitTypeDef RCC_OscInitStruct;

    /* Enable Power Control clock */
    __HAL_RCC_PWR_CLK_ENABLE();

    /* The voltage scaling allows optimizing the power consumption when the device is
       clocked below the maximum system frequency, to update the voltage scaling value
       regarding system frequency refer to product datasheet.  */
    __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);

    /* Enable HSE Oscillator and activate PLL with HSE as source */
    RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
    RCC_OscInitStruct.HSEState = RCC_HSE_ON;
    RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
    RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
    RCC_OscInitStruct.PLL.PLLM = 8;
    RCC_OscInitStruct.PLL.PLLN = 288;
    RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;
    RCC_OscInitStruct.PLL.PLLQ = 6;
    //RCC_OscInitStruct.PLL.PLLM = 25;
    //RCC_OscInitStruct.PLL.PLLN = 336;
    //RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;
    //RCC_OscInitStruct.PLL.PLLQ = 7;
    HAL_RCC_OscConfig(&RCC_OscInitStruct);

    /* Select PLL as system clock source and configure the HCLK, PCLK1 and PCLK2
       clocks dividers */
    RCC_ClkInitStruct.ClockType = (RCC_CLOCKTYPE_SYSCLK | RCC_CLOCKTYPE_HCLK | RCC_CLOCKTYPE_PCLK1 | RCC_CLOCKTYPE_PCLK2);
    RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
    RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
    RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV4;
    RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV2;
    HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_5);

    /* STM32F405x/407x/415x/417x Revision Z devices: prefetch is supported  */
    if (HAL_GetREVID() == 0x1001)
    {
        /* Enable the Flash prefetch */
        __HAL_FLASH_PREFETCH_BUFFER_ENABLE();
    }
}

/**
  * @brief  This function is executed in case of error occurrence.
  * @param  None
  * @retval None
  */
static void Error_Handler(void)
{

    while(1)
    {
    }
}

/**
  * @brief  Configures the CAN.
  * @param  None
  * @retval None
  */
static uint8_t CAN_Config(void)
{
    CAN_FilterConfTypeDef  sFilterConfig;
    static CanTxMsgTypeDef        TxMessage;
    static CanRxMsgTypeDef        RxMessage;

    /*##-1- Configure the CAN peripheral #######################################*/
    CanHandle.Instance = CAN1;
    CanHandle.pTxMsg = &TxMessage;
    CanHandle.pRxMsg = &RxMessage;

    CanHandle.Init.TTCM = DISABLE;
    CanHandle.Init.ABOM = DISABLE;
    CanHandle.Init.AWUM = DISABLE;
    CanHandle.Init.NART = DISABLE;
    CanHandle.Init.RFLM = DISABLE;
    CanHandle.Init.TXFP = DISABLE;
    CanHandle.Init.Mode = CAN_MODE_NORMAL;
    CanHandle.Init.SJW = CAN_SJW_1TQ;
    CanHandle.Init.BS1 = CAN_BS1_6TQ;
    CanHandle.Init.BS2 = CAN_BS2_8TQ;
    CanHandle.Init.Prescaler = 2;

    if(HAL_CAN_Init(&CanHandle) != HAL_OK)
    {
        /* Initialization Error */
        //Error_Handler();
        return 0;
    }

    /*##-2- Configure the CAN Filter ###########################################*/
    sFilterConfig.FilterNumber = 0;
    sFilterConfig.FilterMode = CAN_FILTERMODE_IDMASK;
    sFilterConfig.FilterScale = CAN_FILTERSCALE_32BIT;
    sFilterConfig.FilterIdHigh = 0x0000;
    sFilterConfig.FilterIdLow = 0x0000;
    sFilterConfig.FilterMaskIdHigh = 0x0000;
    sFilterConfig.FilterMaskIdLow = 0x0000;
    sFilterConfig.FilterFIFOAssignment = 0;
    sFilterConfig.FilterActivation = ENABLE;
    sFilterConfig.BankNumber = 14;

    if(HAL_CAN_ConfigFilter(&CanHandle, &sFilterConfig) != HAL_OK)
    {
        /* Filter configuration Error */
        //Error_Handler();
        return 0;
    }

    /*##-3- Configure Transmission process #####################################*/
    CanHandle.pTxMsg->StdId = 0x321;
    CanHandle.pTxMsg->ExtId = 0x01;
    CanHandle.pTxMsg->RTR = CAN_RTR_DATA;
    CanHandle.pTxMsg->IDE = CAN_ID_STD;
    CanHandle.pTxMsg->DLC = 2;
    return 1;
}

/**
  * @brief  Transmission  complete callback in non blocking mode
  * @param  CanHandle: pointer to a CAN_HandleTypeDef structure that contains
  *         the configuration information for the specified CAN.
  * @retval None
  */
void HAL_CAN_RxCpltCallback(CAN_HandleTypeDef* CanHandle)
{
    if ((CanHandle->pRxMsg->StdId == 0x321)&&(CanHandle->pRxMsg->IDE == CAN_ID_STD) && (CanHandle->pRxMsg->DLC == 2))
    {
        LED_Display(CanHandle->pRxMsg->Data[0]);
        ubKeyNumber = CanHandle->pRxMsg->Data[0];
    }

    /* Receive */
    if(HAL_CAN_Receive_IT(CanHandle, CAN_FIFO0) != HAL_OK)
    {
        /* Reception Error */
        Error_Handler();
    }
}

/**
  * @brief  Turn ON/OFF the dedicated LED.
  * @param  Ledstatus: Led number from 0 to 3.
  * @retval None
  */
void LED_Display(uint8_t Ledstatus)
{
    /* Turn off all LEDs */
    BSP_LED_Off(LED1);
    BSP_LED_Off(LED2);
    BSP_LED_Off(LED3);
    BSP_LED_Off(LED4);

    switch(Ledstatus)
    {
    case(1):
        BSP_LED_On(LED1);
        break;

    case(2):
        BSP_LED_On(LED2);
        break;

    case(3):
        BSP_LED_On(LED3);
        break;

    case(4):
        BSP_LED_On(LED4);
        break;

    default:
        break;
    }
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t* file, uint32_t line)
{
    /* User can add his own implementation to report the file name and line number,
       ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */

    /* Infinite loop */
    while (1)
    {
    }
}
#endif

/**
  * @}
  */

/**
  * @}
  */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
