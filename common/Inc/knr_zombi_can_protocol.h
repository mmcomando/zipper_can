#ifndef KNR_ZOMBI_CAN_PROTOCOL_H_INCLUDED
#define KNR_ZOMBI_CAN_PROTOCOL_H_INCLUDED

#ifdef STM32F373xC
#include "stm32f3xx_hal.h"
#else
#include "stm32f4xx_hal.h"
#endif

extern CAN_HandleTypeDef    CanHandle;


typedef void (*RemoveHandler)(void);

enum CAN_EXT_ID
{
    CAN_EXT_ID_NONE=0,//==0
    //for size<=8
    CAN_EXT_ID_SINGLE=1,
    //for size>8
    CAN_EXT_ID_STREAM_START=02,
    CAN_EXT_ID_STREAM_CONTINUE=3,
    CAN_EXT_ID_STREAM_END=4
};
typedef struct CAN_Message
{
    RemoveHandler removeFunctionPointer;
    char* data;
    struct CAN_Message* previous;
    struct CAN_Message* next;
    uint16_t length;

} CAN_Message;


void addMessage(char* data,uint16_t length,void (*removeFunctionPointer)());
CAN_Message* popTailMessage(void);
CAN_Message* popHeadMessage(void);
void removeMessage(CAN_Message* msg);
uint8_t sendData(char* data,uint16_t length);
uint8_t sendSingle(char* data,uint16_t length);

char* receiveData(CAN_HandleTypeDef *CanHandle,uint16_t *return_length);


#endif // KNR_ZOMBI_CAN_PROTOCOL_H_INCLUDED
