#include "knr_zombi_can_protocol.h"
#include <stdlib.h>
#include <string.h>
#include <assert.h>

extern CAN_HandleTypeDef    CanHandle;
CAN_Message*volatile head = NULL;
CAN_Message*volatile tail = NULL;
//Not sure if there ahe no race conditions with interrupts
void addMessage(char* data,uint16_t length,RemoveHandler removeFunctionPointer)
{

    CAN_Message* pp;
    pp = malloc(sizeof(CAN_Message));
    pp->removeFunctionPointer=removeFunctionPointer;
    pp->data=data;
    pp->previous=NULL;
    pp->next=head;
    pp->length=length;

    if(head!=NULL)
    {
        head->previous=pp;
    }
    else
    {
        tail=pp;
    }

    head=pp;

    //return head;
}
CAN_Message* popTailMessage(void)
{
    CAN_Message* to_return=tail;
    if(to_return!=NULL)
    {
        if(to_return->previous==NULL){
            head=NULL;
        }
        tail=to_return->previous;

    }
    return to_return;
}
/*uint8_t sendMessage(CAN_Message* msg)
{
    uint8_t ok;
    do
    {
        ok=sendData(msg->data,msg->length);
    }
    while(ok==0);
    return 1;
}*/
/*
//there certainly might be race conditions
CAN_Message* popHeadMessage(void)
{
    CAN_Message* to_return=head;
    if(to_return!=NULL)
    {
        head=to_return->next;
    }
    return to_return;
}*/
void removeMessage(CAN_Message* msg)
{
    if(msg->removeFunctionPointer!=NULL)
    {
        msg->removeFunctionPointer();
    }
    free(msg);
}


uint8_t sendData(char* data,uint16_t length)
{
    if(length<=8)
    {
        return  sendSingle(data,length);
    }

    //HAL_Delay(4000);//For debug you can see each message in debugger
    assert(length>8);

    CanHandle.pTxMsg->ExtId = 0xFF;
    CanHandle.pTxMsg->RTR = CAN_RTR_DATA;
    CanHandle.pTxMsg->IDE = CAN_ID_STD;

    //start frame
    CanHandle.pTxMsg->StdId = CAN_EXT_ID_STREAM_START;
    CanHandle.pTxMsg->DLC = 2;//uint16_t length
    memcpy(CanHandle.pTxMsg->Data,&length,2);
    if(HAL_CAN_Transmit(&CanHandle, 10) != HAL_OK)
    {
        return 0;
    }

    //stream data
    uint16_t length_sent=0;
    while(length!=0)
    {
        //HAL_Delay(4000);
        uint8_t to_send=8;
        if(length<8)
        {
            to_send=length;
        }
        length-=to_send;
        CanHandle.pTxMsg->StdId = CAN_EXT_ID_STREAM_CONTINUE;
        CanHandle.pTxMsg->DLC = to_send;
        memcpy(CanHandle.pTxMsg->Data,data+length_sent,to_send);
        length_sent+=to_send;
        if(HAL_CAN_Transmit(&CanHandle, 10) != HAL_OK)
        {
            return 0;
        }
    }

    //HAL_Delay(4000);
    //end frame
    CanHandle.pTxMsg->StdId = CAN_EXT_ID_STREAM_END;
    CanHandle.pTxMsg->DLC = 4;//uint32_t zeros
    uint32_t zeros=0;
    memcpy(CanHandle.pTxMsg->Data,&zeros,4);
    if(HAL_CAN_Transmit(&CanHandle, 10) != HAL_OK)
    {
        return 0;
    }
    //HAL_Delay(4000);
    return 1;
}
uint8_t sendSingle(char* data,uint16_t length)
{
    assert(length<=8);
    CanHandle.pTxMsg->StdId = CAN_EXT_ID_SINGLE;
    CanHandle.pTxMsg->ExtId = 0xFF;
    CanHandle.pTxMsg->RTR = CAN_RTR_DATA;
    CanHandle.pTxMsg->IDE = CAN_ID_STD;

    CanHandle.pTxMsg->DLC = length;
    memcpy(CanHandle.pTxMsg->Data,data,length);

    if(HAL_CAN_Transmit(&CanHandle, 10) != HAL_OK)
    {
        return 0;
    }
    return 1;
}

/////// Receive data



char* data=NULL;
uint16_t data_length=0;
uint16_t received_length=0;
uint8_t last_ended=1;

static void cleanReceiveData(void)
{
    free(data);
    data=NULL;
    data_length=0;
    received_length=0;
    last_ended=1;
}

///You need to free the message
char* receiveData(CAN_HandleTypeDef *CanHandle,uint16_t *return_length)
{

    char* tmp_data;

    CanRxMsgTypeDef* msg=CanHandle->pRxMsg;
    uint16_t packet_length=msg->DLC;
    switch(msg->StdId)
    {
    case CAN_EXT_ID_SINGLE:
        tmp_data=malloc(packet_length);
        memcpy(tmp_data,msg->Data,packet_length);
        *return_length=packet_length;
        return tmp_data;
    case CAN_EXT_ID_STREAM_START:
        if(!last_ended)
        {
            cleanReceiveData();
            // WarningHandler();
        }
        memcpy(&data_length,msg->Data,2);
        data=malloc(data_length);
        last_ended=0;
        return NULL;
    case CAN_EXT_ID_STREAM_CONTINUE:
        if(received_length+packet_length>data_length)
        {
            cleanReceiveData();
            //WarningHandler();
        }
        memcpy(data+received_length,msg->Data,packet_length);

        received_length+=packet_length;
        return NULL;
    case CAN_EXT_ID_STREAM_END:
        tmp_data=data;
        *return_length=data_length;
        data=NULL;
        data_length=0;
        received_length=0;
        last_ended=1;
        return tmp_data;
    default:
        return NULL;
    }
}



